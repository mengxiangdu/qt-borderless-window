# QtBorderlessWindow

#### 介绍
用纯Qt5.9实现的无边框窗口

#### 软件架构
类MBorderlessWidget是实现无边框窗口的类。实现了窗口边缘阴影；鼠标拖拽调整窗口尺寸；双击空白区域（标题）最大化/恢复窗口的功能。代码总共就200多行很容易看懂。后续又加了拖拽调整布局的功能可以参考


#### 安装教程

1.  不需要安装

#### 使用说明

1.  本程序开发环境是VS2017/VS2015、Qt5.9
2.  下载即用没有其他依赖库，也没用Windows API
3.  我有时候用VS2015，有时候用VS2017。这俩编译器在转换的时候可能会导致项目无法编译。如果出现链接错误可以在项目属性里找C/C++→代码生成→运行库设置为多线程调试DLL（Debug版）或多线程DLL（Release版）

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
