/////////////////////////////////////////////////////////////////////////////////////////
// 这是实现无边框窗口的类。如果是主窗口，可以把它的centralWidget提升为此类；如果是一
// 般窗口，直接把此widget作为顶层窗口弹出显示就行了；或者写一个子类继承此类也行。
// 制造透明的原理是设置窗口的Qt::WA_TranslucentBackground属性，这个属性在Qt帮助中说明
// 是相当于把窗口增加了一个透明通道，意思就是你可以在绘制窗口时任何地方都能有透明度。
// 不仅边缘能透明，在窗口中间挖个窟窿也是可以的。
/////////////////////////////////////////////////////////////////////////////////////////
#pragma once

#include "qwidget.h"

class MBorderlessWidget : public QWidget
{
    Q_OBJECT

private:
    enum MouseAlign
    {
        POS_NOWHERE = 0,
        POS_LEFT = 1,
        POS_RIGHT = 2,
        POS_TOP = 4,
        POS_BOTTOM = 8,
        POS_CAPITAL = 16,
    };

public:
    MBorderlessWidget(QWidget* parent = 0);

private:
    void paintEvent(QPaintEvent* event) override;
    virtual void paintBackground(QPainter* painter, const QRect& content);
    void mouseMoveEvent(QMouseEvent* event) override;
    void mousePressEvent(QMouseEvent *event) override;
    void mouseReleaseEvent(QMouseEvent *event) override;
    void mouseDoubleClickEvent(QMouseEvent *event) override;
    bool eventFilter(QObject* src, QEvent *event) override;
    void tryChangeCursor(int mouseAlign);
    QRect calcNewGeometry(QMouseEvent* event, QPoint oldPos);
    int calcCursorAlign(QPoint pos);

private:
    QWidget* topLevel;
    QPoint lastPos;
    bool firstMove;
    int alignType;
    int margin; /* 阴影宽度 */
};





