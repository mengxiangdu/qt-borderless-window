#pragma once

#include <QtWidgets/QMainWindow>
#include "ui_QtBorderlessWindow.h"

class QtBorderlessWindow : public QMainWindow
{
    Q_OBJECT

public:
    QtBorderlessWindow(QWidget *parent = nullptr);
    ~QtBorderlessWindow();

private:
    void changeEvent(QEvent* event) override;
	bool eventFilter(QObject* src, QEvent* event) override;
	QPixmap createDragPixmap(QWidget* widget);
	void titleLabelHandler(QWidget* title, QEvent* event);
	void dragOverContent(QEvent* event);

private slots:
    void on_pbMin_clicked();
    void on_pbMax_clicked();
    void on_pbClose_clicked();

private:
    Ui::QtBorderlessWindowClass ui;
	QPoint pressed;
	QPoint savedIndex[2]; /* 指示ui.wiHor,ui.wiVer的索引 */
	QPoint dragIndex[2]; /* 拖拽时暂存ui.wiHor,ui.wiVer的索引 */
};

struct MyDragObject
{
public:
    MyDragObject(QWidget* object = 0);
    QByteArray toByteArray() const;
    static MyDragObject fromByteArray(const QByteArray& data);

public:
    QWidget* ptr;
};

Q_DECLARE_METATYPE(MyDragObject)

QDataStream &operator<<(QDataStream &out, const MyDragObject &myObj);

QDataStream &operator>>(QDataStream &in, MyDragObject &myObj);


