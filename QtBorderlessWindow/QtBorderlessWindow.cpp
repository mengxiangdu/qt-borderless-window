/////////////////////////////////////////////////////////////////////////////////////////
// 这里实现了ui.wiHor和ui.wiVer两个控件的拖拽调整布局的功能
/////////////////////////////////////////////////////////////////////////////////////////
#include "QtBorderlessWindow.h"
#include "MBorderlessWidget.h"
#include "qevent.h"
#include "qmimedata.h"
#include "qdrag.h"
#include "qgraphicseffect.h"
#include "qpainter.h"
#include "qdebug.h"

QtBorderlessWindow::QtBorderlessWindow(QWidget *parent)
    : QMainWindow(parent)
{
    ui.setupUi(this);

	savedIndex[0] = QPoint(0, 0);
	savedIndex[1] = QPoint(0, 1);
	ui.lbHorTitle->installEventFilter(this);
	ui.lbVerTitle->installEventFilter(this);
	ui.wiContent->installEventFilter(this);
}

QtBorderlessWindow::~QtBorderlessWindow()
{
}

void QtBorderlessWindow::on_pbMin_clicked()
{
    showMinimized();
}

void QtBorderlessWindow::on_pbMax_clicked()
{
    isMaximized() ? showNormal() :
        showMaximized();
}

void QtBorderlessWindow::on_pbClose_clicked()
{
    close();
}

void QtBorderlessWindow::changeEvent(QEvent* event)
{
    if (event->type() == QEvent::WindowStateChange)
    {
        QIcon icon = (windowState() & Qt::WindowMaximized) ? QIcon(":/QtBorderlessWindow/image/rec.png") : 
            QIcon(":/QtBorderlessWindow/image/max.png");
        ui.pbMax->setIcon(icon);
    }
}

bool QtBorderlessWindow::eventFilter(QObject* src, QEvent* event)
{
	if (src == ui.lbHorTitle)
	{
		titleLabelHandler(ui.lbHorTitle, event);
	}
	else if (src == ui.lbVerTitle)
	{
		titleLabelHandler(ui.lbVerTitle, event);
	}
	else if (src == ui.wiContent)
	{
		dragOverContent(event);
	}
	return false;
}

#define MY_MIME_TYPE u8R"(application/x-qt-windows-mime;value="QWidget*")"

void QtBorderlessWindow::titleLabelHandler(QWidget* title, QEvent* event)
{
	switch (event->type())
	{
	case QEvent::MouseButtonPress:
		pressed = dynamic_cast<QMouseEvent*>(event)->pos();
		break;
	case QEvent::MouseButtonRelease:
		pressed = QPoint(-1, -1);
		break;
	case QEvent::MouseMove: {
		QPoint curr = dynamic_cast<QMouseEvent*>(event)->pos();
		if (pressed.x() >= 0 && (curr - pressed).manhattanLength() > QApplication::startDragDistance())
		{
			QDrag drag(this);
			QMimeData* mimeData = new QMimeData;
			if (title == ui.lbHorTitle)
			{
				ui.glHost->removeWidget(ui.wiHor);
                mimeData->setData(MY_MIME_TYPE, MyDragObject(ui.wiHor).toByteArray());
				drag.setPixmap(createDragPixmap(ui.wiHor));
			}
			else if (title == ui.lbVerTitle)
			{
				ui.glHost->removeWidget(ui.wiVer);
                mimeData->setData(MY_MIME_TYPE, MyDragObject(ui.wiVer).toByteArray());
				drag.setPixmap(createDragPixmap(ui.wiVer));
			}
			drag.setMimeData(mimeData);
			if (Qt::IgnoreAction == drag.exec())
			{
                /* 取消了拖拽那么恢复原有的布局 */
				MyDragObject object = MyDragObject::fromByteArray(mimeData->data(MY_MIME_TYPE));
                object.ptr->setGraphicsEffect(0);
			}
		}
		break;
	}
	default:
		break;
	}
}

void QtBorderlessWindow::dragOverContent(QEvent* event)
{
	switch (event->type())
	{
	case QEvent::DragEnter:
		dynamic_cast<QDragEnterEvent*>(event)->acceptProposedAction();
		break;
	case QEvent::DragMove: {
		QDragMoveEvent* moving = dynamic_cast<QDragMoveEvent*>(event);
		const QMimeData* data = moving->mimeData();
		MyDragObject which = MyDragObject::fromByteArray(data->data(MY_MIME_TYPE));
		QWidget* other = (which.ptr == ui.wiHor ? ui.wiVer : ui.wiHor);
		
		QGraphicsOpacityEffect* effect = new QGraphicsOpacityEffect(which.ptr);
		effect->setOpacity(0.5);
		which.ptr->setGraphicsEffect(effect);

		QPoint movePos = moving->pos();
		if (movePos.y() < 50) /* 在上方 */
		{
			ui.glHost->removeWidget(other);
			ui.glHost->addWidget(which.ptr, 0, 0);
			ui.glHost->addWidget(other, 1, 0);
			which.ptr == ui.wiHor ? dragIndex[0] = QPoint(0, 0) : dragIndex[1] = QPoint(0, 0);
			which.ptr == ui.wiHor ? dragIndex[1] = QPoint(0, 1) : dragIndex[0] = QPoint(0, 1);
			moving->acceptProposedAction();
		}
		else if (movePos.y() > ui.wiContent->height() - 50) /* 在下方 */
		{
			ui.glHost->removeWidget(other);
			ui.glHost->addWidget(which.ptr, 1, 0);
			ui.glHost->addWidget(other, 0, 0);
			which.ptr == ui.wiHor ? dragIndex[0] = QPoint(0, 1) : dragIndex[1] = QPoint(0, 1);
			which.ptr == ui.wiHor ? dragIndex[1] = QPoint(0, 0) : dragIndex[0] = QPoint(0, 0);
			moving->acceptProposedAction();
		}
		else if (movePos.x() < 50) /* 在左方 */
		{
			ui.glHost->removeWidget(other);
			ui.glHost->addWidget(which.ptr, 0, 0);
			ui.glHost->addWidget(other, 0, 1);
			which.ptr == ui.wiHor ? dragIndex[0] = QPoint(0, 0) : dragIndex[1] = QPoint(0, 0);
			which.ptr == ui.wiHor ? dragIndex[1] = QPoint(1, 0) : dragIndex[0] = QPoint(1, 0);
			moving->acceptProposedAction();
		}
		else if (movePos.x() > ui.wiContent->width() - 50) /* 在右方 */
		{
			ui.glHost->removeWidget(other);
			ui.glHost->addWidget(which.ptr, 0, 1);
			ui.glHost->addWidget(other, 0, 0);
			which.ptr == ui.wiHor ? dragIndex[0] = QPoint(1, 0) : dragIndex[1] = QPoint(1, 0);
			which.ptr == ui.wiHor ? dragIndex[1] = QPoint(0, 0) : dragIndex[0] = QPoint(0, 0);
			moving->acceptProposedAction();
		}
		else /* 在中间 */
		{
			ui.glHost->removeWidget(ui.wiHor);
			ui.glHost->removeWidget(ui.wiVer);
			ui.glHost->addWidget(ui.wiHor, savedIndex[0].y(), savedIndex[0].x());
			ui.glHost->addWidget(ui.wiVer, savedIndex[1].y(), savedIndex[1].x());
			moving->ignore();
		}
		break;
	}
	case QEvent::DragLeave: {
		ui.glHost->removeWidget(ui.wiHor);
		ui.glHost->removeWidget(ui.wiVer);
		ui.glHost->addWidget(ui.wiHor, savedIndex[0].y(), savedIndex[0].x());
		ui.glHost->addWidget(ui.wiVer, savedIndex[1].y(), savedIndex[1].x());
		break;
	}
	case QEvent::Drop: {
		QDropEvent* drop = dynamic_cast<QDropEvent*>(event);
		const QMimeData* data = drop->mimeData();
        MyDragObject which = MyDragObject::fromByteArray(data->data(MY_MIME_TYPE));
        /* 拖拽结束暂存的布局将生效 */
		savedIndex[0] = dragIndex[0];
		savedIndex[1] = dragIndex[1];
		which.ptr->setGraphicsEffect(0);
		break;
	}
	default:
		break;
	}
}

QPixmap QtBorderlessWindow::createDragPixmap(QWidget* widget)
{
	QPixmap src = widget->grab();
	QPixmap target = src.scaled(QSize(240, 240), Qt::KeepAspectRatio);
	QPixmap background(target.size());
	background.fill(Qt::transparent);
	QPainter painter(&background);
	painter.setOpacity(0.5);
	painter.drawPixmap(0, 0, target);
	return background;
}

#undef MY_MIME_TYPE

/////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////

QDataStream &operator<<(QDataStream &out, const MyDragObject &myObj)
{
    out << (quint64)myObj.ptr;
    return out;
}

QDataStream &operator>>(QDataStream &in, MyDragObject &myObj)
{
    quint64 ptr;
    in >> ptr;
    myObj.ptr = (QWidget*)ptr;
    return in;
}

MyDragObject::MyDragObject(QWidget* object)
{
    ptr = object;
}

QByteArray MyDragObject::toByteArray() const
{
    QByteArray data;
    QDataStream ds(&data, QIODevice::WriteOnly);
    ds << *this;
    return data;
}

MyDragObject MyDragObject::fromByteArray(const QByteArray& data)
{
    QDataStream ds(data);
    MyDragObject object;
    ds >> object;
    return object;
}


