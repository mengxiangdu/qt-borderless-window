#include "QtBorderlessWindow.h"
#include <QtWidgets/QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    QtBorderlessWindow w;
    w.show();
    return a.exec();
}
