#include "MBorderlessWidget.h"
#include "qpainter.h"
#include "qlayout.h"
#include "qevent.h"
#include "qdebug.h"

#define DEFAULT_SHADOW 9

MBorderlessWidget::MBorderlessWidget(QWidget* parent) :
    QWidget(parent)
{
    topLevel = topLevelWidget();
    topLevel->setWindowFlag(Qt::FramelessWindowHint);
    topLevel->setAttribute(Qt::WA_TranslucentBackground);
    topLevel->installEventFilter(this);
    setMouseTracking(true);
    lastPos = QPoint(-1, -1);
    alignType = 0;
    margin = DEFAULT_SHADOW;
    firstMove = false;
}

void MBorderlessWidget::paintEvent(QPaintEvent* event)
{
    QPainter painter(this);
    int whole = 2 * margin;
    QRect content(margin, margin, width() - whole, height() - whole);
    /* 此处绘制透明的边框 */
    for (int i = whole; i >= 1; i -= 2)
    {
        painter.setPen(QPen(QColor(24, 24, 24, (whole - i + 1) * 1), i, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
        painter.drawRect(content);
    }
    painter.setClipRect(content);
    paintBackground(&painter, content);
}

void MBorderlessWidget::paintBackground(QPainter* painter, const QRect& content)
{
    /* 如果需要自定义背景在这里绘制或重写此函数绘制 */
    /* 不能用QSS样式表定义背景，因QSS效果会覆盖此处绘制的内容 */
    painter->fillRect(content, QColor(46, 138, 236));
}

void MBorderlessWidget::mouseMoveEvent(QMouseEvent* event)
{
    if (topLevel->isMaximized())
    {
        firstMove = false;
        releaseMouse();
        return;
    }
    if (event->buttons() & Qt::LeftButton) /* 按下鼠标左键 */
    {
        QRect newRect = calcNewGeometry(event, lastPos);
        topLevel->setGeometry(newRect);
        lastPos = event->globalPos();
    }
    else /* 未按下鼠标左键 */
    {
        /* 我们在第一次收到MouseMove消息时捕获鼠标 */
        /* 在鼠标不在任何边框位置时释放鼠标 */
        /* 之所以不在enterEvent和leaveEvent事件中捕获/释放 */
        /* 鼠标是因为这俩事件会在鼠标拖动时多次触发 */
        if (!firstMove)
        {
            firstMove = true;
            grabMouse();
        }
        int align = calcCursorAlign(event->pos());
        tryChangeCursor(align);
        if (align == 0)
        {
            firstMove = false;
            releaseMouse();
        }
    }
}

void MBorderlessWidget::tryChangeCursor(int mouseAlign)
{
    switch (mouseAlign)
    {
    case POS_LEFT:
    case POS_RIGHT:
        setCursor(Qt::SizeHorCursor);
        break;
    case POS_TOP:
    case POS_BOTTOM:
        setCursor(Qt::SizeVerCursor);
        break;
    case POS_LEFT | POS_TOP:
    case POS_RIGHT | POS_BOTTOM:
        setCursor(Qt::SizeFDiagCursor);
        break;
    case POS_RIGHT | POS_TOP:
    case POS_LEFT | POS_BOTTOM:
        setCursor(Qt::SizeBDiagCursor);
        break;
    default:
        unsetCursor();
    }
}

int MBorderlessWidget::calcCursorAlign(QPoint pos)
{
    int type = 0;
    if (pos.x() < margin)
    {
        type |= POS_LEFT;
    }
    if (pos.x() > width() - margin)
    {
        type |= POS_RIGHT;
    }
    if (pos.y() < margin)
    {
        type |= POS_TOP;
    }
    if (pos.y() > height() - margin)
    {
        type |= POS_BOTTOM;
    }
    QWidget* child = childAt(pos);
    if (!type && !child)
    {
        type |= POS_CAPITAL;
    }
    return type;
}

QRect MBorderlessWidget::calcNewGeometry(QMouseEvent* event, QPoint oldPos)
{
    QPoint newPos = event->globalPos();
    QRect newRect = topLevel->geometry();
    switch (alignType)
    {
    case POS_LEFT:
        newRect.adjust(newPos.x() - oldPos.x(), 0, 0, 0);
        break;
    case POS_RIGHT:
        newRect.adjust(0, 0, newPos.x() - oldPos.x(), 0);
        break;
    case POS_TOP:
        newRect.adjust(0, newPos.y() - oldPos.y(), 0, 0);
        break;
    case POS_BOTTOM:
        newRect.adjust(0, 0, 0, newPos.y() - oldPos.y());
        break;
    case POS_LEFT | POS_TOP:
        newRect.adjust(newPos.x() - oldPos.x(), newPos.y() - oldPos.y(), 0, 0);
        break;
    case POS_RIGHT | POS_TOP:
        newRect.adjust(0, newPos.y() - oldPos.y(), newPos.x() - oldPos.x(), 0);
        break;
    case POS_LEFT | POS_BOTTOM:
        newRect.adjust(newPos.x() - oldPos.x(), 0, 0, newPos.y() - oldPos.y());
        break;
    case POS_RIGHT | POS_BOTTOM:
        newRect.adjust(0, 0, newPos.x() - oldPos.x(), newPos.y() - oldPos.y());
        break;
    case POS_CAPITAL: {
        int deltaX = newPos.x() - oldPos.x();
        int deltaY = newPos.y() - oldPos.y();
        newRect.adjust(deltaX, deltaY, deltaX, deltaY);
        break;
    }
    default:
        break;
    }
    return newRect;
}

void MBorderlessWidget::mouseDoubleClickEvent(QMouseEvent *event)
{
    int type = calcCursorAlign(event->pos());
    if (type == POS_CAPITAL)
    {
        topLevel->isMaximized() ? topLevel->showNormal() : 
            topLevel->showMaximized();
    }
}

void MBorderlessWidget::mousePressEvent(QMouseEvent *event)
{
    lastPos = event->globalPos();
    alignType = calcCursorAlign(event->pos());
    tryChangeCursor(alignType);
}

void MBorderlessWidget::mouseReleaseEvent(QMouseEvent *event)
{
    lastPos = QPoint(-1, -1);
    alignType = 0;
}

bool MBorderlessWidget::eventFilter(QObject* src, QEvent *event)
{
    if (event->type() == QEvent::WindowStateChange)
    {
        /* 这里对阴影边的宽度赋值，最大化后margin为0 */
        margin = (topLevel->windowState() & Qt::WindowMaximized) ? 0 : DEFAULT_SHADOW;
        QLayout* lay = layout();
        /* 设置布局的MARGINS，在初始化时MARGINS也应设置为DEFAULT_SHADOW */
        /* 要不然显示会有白色边 */
        lay->setContentsMargins(margin, margin, margin, margin);
        update();
    }
    return false;
}






